package br.com.infox.dal;

import java.sql.*;

/**
 *
 * @author Luiz Souza
 */
public class ModuloConexao {
    /*     Método responsavel por estabelecer a conexao com o banco */

    public static Connection conector() {
        java.sql.Connection conexao = null;
        /*         chamando o driver de conexao         */
        String driver = "com.mysql.jdbc.Driver";
        /*         Armazenando informações referentes ao banco de dados         */
        String url = "jdbc:mysql://localhost:3306/dbinfox";
        String user = "root";
        String password = "";
        /*        Estabelecendo a conexao com o banco        */
        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, user, password);
            return conexao;
        } catch (Exception e) {
            /*
            A linha abaixo serve de apoio para esclarecer o erro
            System.out.println(e);
            */
            
            return null;
        }
    }
}
